# # ---Print Example---
# print('hello world')
#
# # ---Variable Example---
# nzt = input('Enter your name: ')
# print('Hello', nzt)
#
# # ---Expression Example---
# xh = input("Enter Hours: ")
# xr = input("Enter Rate: ")
# xp = float(xh) * float(xr)
# print("Pay:", xp)

# # ---Conditional Example---
# sh = input("Enter Hours: ")
# sr = input("Enter Rate: ")
# fh = float(sh)
# fr = float(sr)
# if fh > 40 :
#     reg = fr * fh
#     otp = (fh - 40.0) * (fr * 0.5)
#     xp = reg + otp
# else:
#     xp = fh * fr
# print("Pay:", xp)

# # ---Conditional with Error Handling Example---
# sh = input("Enter Hours: ")
# sr = input("Enter Rate: ")
# try:
#     fh = float(sh)
#     fr = float(sr)
# except:
#     print("Error, please enter numeric input")
#     quit()
#
# if fh > 40 :
#     reg = fr * fh
#     otp = (fh - 40.0) * (fr * 0.5)
#     xp = reg + otp
# else:
#     xp = fh * fr
# print("Pay:", xp)

# # ---Conditional with Error Handling Example---
# score = input("Enter Score: ")
# try:
#     floatScore = float(score)
# except:
#     print("Please enter a numeric value")
#     quit()
#
# if floatScore < 0 or floatScore > 1:
#     print("Score out of range")
# elif floatScore >= 0.9:
#     print("A")
# elif floatScore >= 0.8:
#     print("B")
# elif floatScore >= 0.7:
#     print("C")
# elif floatScore >= 0.6:
#     print("D")
# else:
#     print("F")
