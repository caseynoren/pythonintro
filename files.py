# # ---File Example---
fh = open('mbox-short.txt')

for lx in fh :
    ly = lx.rstrip()
    print(ly)

# # ---File Average Extraction Example---
fname = input("Enter file name: ")
fh = open(fname)
count = 0
value = 0
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    count = count + 1
    colonLocation = line.find(':')
    lineWithoutLetters = line[colonLocation+1:]
    value = value + float(lineWithoutLetters.rstrip())
print("Average spam confidence:", (value/count))
