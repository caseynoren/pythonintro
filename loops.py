# ---Count and Average Loop Example---
num = 0
tot = 0

while True :
    sval = input('Enter a number: ')
    if sval == 'done' :
        break
    try:
        fval = float(sval)
    except:
        print('Invalid input')
        continue
    num = num + 1
    tot = tot + fval

print(tot, num, tot/num)

# ---Largest and Smallest Loop Example---
largest = None
smallest = None

while True:
    num = input("Enter a number: ")
    if num == "done" : break
    try :
        num = float(num)
    except :
        print("Invalid input")
        continue
    if largest is None :
        largest = num
    elif largest < num :
        largest = int(num)
    if smallest is None :
        smallest = num
    elif smallest > num :
        smallest = int(num)

print("Maximum is", largest)
print("Minimum is", smallest)
