# ---Function Example---
def computePay(hours, rate) :
    try:
        fh = float(hours)
        fr = float(rate)
    except:
        print("Error, please enter numeric input")
        quit()

    if fh > 40 :
        reg = fr * fh
        otp = (fh - 40.0) * (fr * 0.5)
        pay = reg + otp
    else:
        pay = fh * fr
    return pay

sh = input("Enter Hours: ")
sr = input("Enter Rate: ")

xp = computePay(sh, sr)

print("Pay:", xp)
